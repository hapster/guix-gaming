;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2023 Sughosha <sughosha@proton.me>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (games packages yooka-laylee)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (games build-system mojo)
  #:use-module (games gog-download)
  #:use-module (gnu packages)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages sdl)
  #:use-module (gnu packages xorg)
  #:use-module (nonguix build utils)
  #:use-module (nonguix licenses))

(define-public gog-yooka-laylee
  (let ((buildno "38486")
        (arch (if (target-64bit?) "x86_64" "i686-linux")))
    (package
      (name "gog-yooka-laylee")
      (version "175")
      (source
       (origin
         (method gog-fetch)
         (uri "gogdownloader://yookalaylee/en3installer0")
         (file-name (string-append "yooka_laylee_"
                                  (string-replace-substring version "." "_")
                                  "_" buildno ".sh"))
         (sha256
          (base32
           "0v0pfkacmchwvshcyx9bw34fv67a1cpbcy63lcm5c48slmqddksc"))))
      (build-system mojo-build-system)
      (arguments
       `(#:patchelf-plan
         `((,,(string-append "YookaLaylee." arch)
            ("libc" "gcc:lib" "alsa-lib" "eudev" "libx11" "libxcursor"
             "libxrandr" "mesa" "pulseaudio"))
           (,,(string-append "YookaLaylee_Data/Mono/"
               arch "/libmono.so")
            ("libc"))
           (,,(string-append "YookaLaylee_Data/Plugins/"
               arch "/libAkFlanger.so")
            ("libc" "gcc:lib"))
           (,,(string-append "YookaLaylee_Data/Plugins/"
               arch "/libAkHarmonizer.so")
            ("libc" "gcc:lib"))
           (,,(string-append "YookaLaylee_Data/Plugins/"
               arch "/libAkSoundEngine.so")
            ("libc" "gcc:lib" "sdl2"))
           (,,(string-append "YookaLaylee_Data/Plugins/"
               arch "/libAkStereoDelay.so")
            ("libc" "gcc:lib"))
           (,,(string-append "YookaLaylee_Data/Plugins/"
               arch "/libCSteamworks.so")
            ("libc"))
           (,,(string-append "YookaLaylee_Data/Plugins/"
               arch "/libsteam_api.so")
            ("libc"))
           (,,(string-append "YookaLaylee_Data/Plugins/"
               arch "/ScreenSelector.so")
            ("libc" "gcc:lib" "glib" "gdk-pixbuf" "gtk+")))
         #:phases
         (modify-phases %standard-phases
           (add-after 'unpack 'unbundle-sdl2
             (lambda _
               (with-directory-excursion "data/noarch/game"
                 (for-each delete-file-recursively
                  (list "lib32" "lib64"))
                 (substitute* "yookalaylee.sh"
                   (("^lib_path") "#lib_path")
                   (("export") "#export")))))
           ;; Delete files of different architecture.
           (add-before 'install 'delete-unneeded-files
             (lambda _
               (with-directory-excursion "data/noarch/game"
                 (delete-file
                  (string-append "YookaLaylee."
                   ,(if (target-64bit?) "x86" "x86_64")))
                 (for-each
                   (lambda (folder)
                     (with-directory-excursion "YookaLaylee_Data"
                       (for-each delete-file-recursively
                        (list (string-append "Mono/" folder)
                              (string-append "Plugins/" folder)))))
                   `(,,(if (target-64bit?) "x86" "x86_64")))))))))
      (inputs
       `(("alsa-lib" ,alsa-lib)
         ("eudev" ,eudev)
         ("gcc:lib" ,gcc "lib")
         ("gdk-pixbuf" ,gdk-pixbuf)
         ("gtk+" ,gtk+-2)
         ("libx11" ,libx11)
         ("libxcursor" ,libxcursor)
         ("libxrandr" ,libxrandr)
         ("mesa" ,mesa)
         ("pulseaudio" ,pulseaudio)
         ("sdl2" ,sdl2)))
      (supported-systems '("i686-linux" "x86_64-linux"))
      (home-page "https://www.playtonicgames.com/game/yooka-laylee")
      (synopsis "Open-world epic adventure game")
      (description "In Yooka-Laylee you can explore huge and beautiful worlds,
meet a cast of characters and horde a vault-load of collectibles as buddy-duo
Yooka and Laylee embark on an epic adventure to thwart corporate creep Capital
B.")
      (license (undistributable
                (string-append "file:///data/noarch/docs/"
                               "End User License Agreement.txt"))))))
