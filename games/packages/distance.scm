;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2022 Sughosha <sughosha@proton.me>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (games packages distance)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix utils)
  #:use-module (gnu packages)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages sdl)
  #:use-module (gnu packages xorg)
  #:use-module (nongnu packages game-development)
  #:use-module (nonguix build utils)
  #:use-module (nonguix build-system binary)
  #:use-module (nonguix licenses)
  #:use-module (games humble-bundle)
  #:use-module (games utils))

(define-public distance
  (let* ((version "6895")
         (file-name (string-append "distance_" version "_linux.tar.gz")))
    (package
      (name "distance")
      (version version)
      (source (origin
                (method humble-bundle-fetch)
                (uri (humble-bundle-reference
                      (help (humble-bundle-help-message name))
                      (config-path (list (string->symbol name) 'key))
                      (files (list file-name))))
                (file-name file-name)
                (sha256
                 (base32
                  "1bmdd5fmh0qfcr4fj9nzcn1giy38qfjf2jw287l03qp3b8ipvzm6"))))
      (build-system binary-build-system)
      (supported-systems '("i686-linux" "x86_64-linux"))
      (arguments
       `(#:system "i686-linux"
         #:patchelf-plan
         `(("Distance"
            ("libc" "alsa-lib" "eudev" "gcc:lib" "libxrandr" "libxcursor"
             "libx11" "mesa"))
           ("Distance_Data/Mono/x86/libmono.so"
             ("libc" "gcc:lib"))
           ("Distance_Data/Mono/x86/libMonoPosixHelper.so"
            ("libc" "zlib"))
           ("Distance_Data/Plugins/x86/libAkFlanger.so"
            ("libc" "gcc:lib"))
           ("Distance_Data/Plugins/x86/libAkGuitarDistortion.so"
            ("libc" "gcc:lib"))
           ("Distance_Data/Plugins/x86/libAkHarmonizer.so"
            ("libc" "gcc:lib"))
           ("Distance_Data/Plugins/x86/libAkPitchShifter.so"
            ("libc" "gcc:lib"))
           ("Distance_Data/Plugins/x86/libAkSoundEngine.so"
            ("libc" "gcc:lib" "sdl2"))
           ("Distance_Data/Plugins/x86/libAkStereoDelay.so"
            ("libc" "gcc:lib"))
           ("Distance_Data/Plugins/x86/libAkTremolo.so"
            ("libc" "gcc:lib"))
           ("Distance_Data/Plugins/x86/libSynthOne.so"
            ("libc" "gcc:lib"))
           ("Distance_Data/Plugins/x86/libCSteamworks.so"
            ("libc" "libsteam"))
           ("Distance_Data/Plugins/x86/ScreenSelector.so"
            ("libc" "gcc:lib" "glib" "gdk-pixbuf" "gtk+")))
         #:install-plan
         `(("Distance" "share/distance/")
           ("Distance_Data" "share/distance/Distance_Data"
            #:exclude ("libsteam_api.so"))
           ("Distance_Data/Resources/UnityPlayer.png"
            "share/pixmaps/distance.png"))
         #:phases
         (modify-phases %standard-phases
           (add-after 'install 'create-wrapper
             (lambda* (#:key inputs outputs #:allow-other-keys)
               (let* ((output (assoc-ref outputs "out"))
                      (share (string-append output "/share"))
                      (wrapper (string-append output "/bin/distance"))
                      (real (string-append share "/distance/Distance")))
                 (make-wrapper wrapper real #:skip-argument-0? #t
                  `("LD_LIBRARY_PATH" suffix
                    (,(string-append share
                       "/distance/Distance_Data/Plugins/x86"))))
                 (make-desktop-entry-file
                  (string-append share
                   "/applications/distance.desktop")
                   #:name "Distance"
                   #:exec wrapper
                   #:icon
                   (string-append share "/pixmaps/distance.png")
                   #:categories '("Application" "Game")))))
           (replace 'install-license-files
             (lambda* (#:key outputs #:allow-other-keys)
               (let* ((output (assoc-ref outputs "out")))
                 (install-file "../EULA.txt"
                  (string-append (assoc-ref outputs "out")
                   "/share/doc/distance"))))))))
      (inputs
       `(("alsa-lib" ,alsa-lib)
         ("eudev" ,eudev)
         ("gcc:lib" ,gcc "lib")
         ("gdk-pixbuf" ,gdk-pixbuf)
         ("glib" ,glib)
         ("gtk+" ,gtk+-2)
         ("libsteam" ,libsteam)
         ("libx11" ,libx11)
         ("libxcursor" ,libxcursor)
         ("libxrandr" ,libxrandr)
         ("mesa" ,mesa)
         ("pulseaudio" ,pulseaudio)
         ("sdl2" ,sdl2)
         ("zlib" ,zlib)))
      (home-page "http://survivethedistance.com/")
      (synopsis "Indie / action / racing game")
      (description
       "Distance is an atmospheric racing platformer.  Fusing futuristic arcade
racing with parkour, survive a deadly, mysterious, neon-drenched city by
jumping, rotating, and flying.")
      (license (undistributable "file:///EULA.txt")))))
