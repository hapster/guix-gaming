;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2023 Eidvilas Markevičius <markeviciuseidvilas@gmail.com>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (games packages gothic-2)
  #:use-module (games gog-download)
  #:use-module (gnu packages base)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages image)
  #:use-module (gnu packages wine)
  #:use-module (guix build-system copy)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (nongnu packages wine)
  #:use-module (nonguix licenses))

(define %gog-gothic-2-version "2.7-win10")

(define gog-gothic-2.exe
  (let ((version %gog-gothic-2-version))
    (origin
      (method gog-fetch)
      (uri "gogdownloader://gothic_2_gold_edition/en1installer0")
      (file-name (string-append "gog-gothic-2-" version ".exe"))
      (sha256 (base32 "1a6lcy8ly0l9wi080dxdkahpf8a53h06c7fcj4sah5msczfmgdh9")))))

(define gog-gothic-2-1.bin
  (let ((version %gog-gothic-2-version))
    (origin
      (method gog-fetch)
      (uri "gogdownloader://gothic_2_gold_edition/en1installer1")
      (file-name (string-append "gog-gothic-2-" version "-1.bin"))
      (sha256 (base32 "0sw67rmlbas41wxr0pkbyyxmqvkng6iri5bjf5az1ylfhg0880yy")))))

(define gothic2-fix.exe
  (let ((version "2.6.0.0-rev2"))
    (origin
      (method url-fetch)
      (uri "https://worldofgothic.de/download.php?id=833")
      (file-name (string-append "gothic2-fix-" version ".exe"))
      (sha256 (base32 "1biqpy3wi5jkw4ibhnw4j8qym9i4nv8mx7szx1mpwgjajx4pjp37")))))

(define g2notr-systempack.exe
  (let ((version "1.8"))
    (origin
      (method url-fetch)
      (uri (string-append
             "https://github.com/GothicFixTeam/GothicFix/releases/download/v"
             version "/G2NoTR-SystemPack-" version ".exe"))
      (file-name (string-append "g2notr-systempack-" version ".exe"))
      (sha256 (base32 "0wg3n1l3qg587vf9d1ns3wrfngb58sd1dif46kfvc1iy8fnc1rdw")))))

(define directx-redist.exe
  (let ((version "feb2010"))
    (origin
      (method url-fetch)
      (uri (string-append
             "https://files.holarse-linuxgaming.de/mirrors/microsoft/directx_"
             version "_redist.exe"))
      (file-name (string-append "directx-" version "-redist.exe"))
      (sha256 (base32 "0gz9l6m7qqa3xirxm4mvscg9rfga947x6sgi6k57qgcnkbl93lgn")))))

(define-public gog-gothic-2
  (package
    (name "gog-gothic-2")
    (version %gog-gothic-2-version)
    (source #f)
    (native-inputs (list innoextract icoutils p7zip))
    (inputs (list coreutils wine winetricks grep sed))
    (build-system copy-build-system)
    (arguments
      (list
        #:phases
        #~(modify-phases %standard-phases
            (replace 'unpack
              (lambda* (#:key inputs #:allow-other-keys)
                ;; Extract the contents of the game itself.
                (symlink #$gog-gothic-2.exe "gog-gothic-2.exe")
                (symlink #$gog-gothic-2-1.bin "gog-gothic-2-1.bin")
                (invoke "innoextract"
                        "--lowercase"
                        "--include" "/app/goggame-1207658718.ico"
                        "--include" "/data"
                        "--include" "/miles"
                        "--include" "/system"
                        "--include" "/_work"
                        "--include" "/vdfs.cfg"
                        "--include" "/manual.pdf"
                        "--include" "/manual_addon.pdf"
                        "gog-gothic-2.exe")

                ;; Extract all the icons from the .ico file.
                (mkdir-p "icons")
                (invoke "icotool" "-x" "-o" "icons" "app/goggame-1207658718.ico")

                ;; Extract the patch for the game's executable and rename files ou.dat and
                ;; ou.lsc to ou.bin and ou.cls, respectively. The renaming step is there
                ;; in order to prevent the issue of missing NPC dialogues that can occur
                ;; after applying the patch.
                (invoke "7z" "e" "-osystem"
                        #$gothic2-fix.exe
                        "System/Gothic2.exe")
                (for-each rename-file
                  '("_work/data/scripts/content/cutscene/ou.dat"
                    "_work/data/scripts/content/cutscene/ou.lsc")
                  '("_work/data/scripts/content/cutscene/ou.bin"
                    "_work/data/scripts/content/cutscene/ou.cls"))

                ;; Extract the "SystemPack" patch.
                (for-each (lambda (dir)
                            (invoke "7z" "e" "-aos"
                                    (string-append "-o" (string-downcase dir))
                                    #$g2notr-systempack.exe
                                    dir))
                  '("Data"
                    "System"
                    "_work/data/Music/NewWorld"))

                ;; For the sake of consistency, convert all the filenames with uppercase
                ;; characters in them to lowercase.
                (for-each (lambda (file)
                            (rename-file file (string-downcase file)))
                  '("system/Gothic2.exe"
                    "data/SystemPack.vdf"
                    "system/Shw32.dll"
                    "system/Vdfs32g.dll"
                    "_work/data/music/newworld/Xardas Tower.sty"
                    "_work/data/music/newworld/XT_DayStd.sgt"))))

            (add-after 'install 'install-wrapper
              (lambda* (#:key inputs outputs #:allow-other-keys)
                (let ((coreutils (assoc-ref inputs "coreutils"))
                      (wine (assoc-ref inputs "wine"))
                      (winetricks (assoc-ref inputs "winetricks"))
                      (grep (assoc-ref inputs "grep"))
                      (sed (assoc-ref inputs "sed"))
                      (out (assoc-ref outputs "out")))
                  (mkdir-p (string-append out "/bin"))
                  (call-with-output-file (string-append out "/bin/gothic2")
                    (lambda (port)
                      (display
                        (string-append
                          "#!/bin/sh\n\n"

                          "export XDG_DATA_HOME="
                            "\"${XDG_DATA_HOME:-$HOME/.local/share}\"\n"
                          "export XDG_CACHE_HOME="
                            "\"${XDG_CACHE_HOME:-$HOME/.cache}\"\n"
                          "export WINEPREFIX="
                            "\"$XDG_DATA_HOME/gothic2/wine\"\n"
                          "export WINEDLLOVERRIDES="
                            "\"mscoree=\"\n"
                          "export PATH=\""
                            coreutils "/bin:"
                            wine "/bin:"
                            winetricks "/bin:"
                            grep "/bin:"
                            sed "/bin\"\n\n"

                          "sourcedir=\"" out "/share/gothic2\"\n"
                          "targetdir=\"$XDG_DATA_HOME/gothic2\"\n"
                          "mkdir -p -m=755 \"$targetdir\"\n"
                          "for f in \"$sourcedir\"/*; do\n"
                          "  if [ \"$f\" = \"$sourcedir/system\" ]; then\n"
                          "    mkdir -p -m=755 \"$targetdir/system\"\n"
                          "  else\n"
                          "    ln -s -f \"$f\" \"$targetdir/\"\n"
                          "  fi\n"
                          "done\n\n"

                          "sourcedir=\"$sourcedir/system\"\n"
                          "targetdir=\"$targetdir/system\"\n"
                          "mkdir -p -m=755 \"$targetdir\"\n"
                          "for f in \"$sourcedir\"/*; do\n"
                          "  if [ \"$f\" = \"$sourcedir/gothic.ini\" ]; then\n"
                          "    if [ ! -f \"$targetdir/gothic.ini\" ] && \\\n"
                          "       [ ! -f \"$targetdir/Gothic.ini\" ]; then\n"
                          "      install -m=644 \"$f\" \"$targetdir/\"\n"
                          "    fi\n"
                          "  else\n"
                          "    ln -s -f \"$f\" \"$targetdir/\"\n"
                          "  fi\n"
                          "done\n\n"

                          ;; This part is necessary to make the in-game music work
                          ;; properly. We use the DirectX executable cached inside
                          ;; /gnu/store in order to prevent winetricks from downloading
                          ;; it during the runtime of the application. This significantly
                          ;; reduces startup time and avoids any errors that might occur
                          ;; if the user lacks internet connectivity when trying to launch
                          ;; the game.
                          "mkdir -p -m=755 \\\n"
                          "  \"$XDG_CACHE_HOME/gothic2/winetricks/directx9\"\n"
                          "ln -s -f \\\n"
                          "  \"" #$directx-redist.exe "\" \\\n"
                          "  \"" "$XDG_CACHE_HOME/gothic2/winetricks/directx9/"
                                 "directx_feb2010_redist.exe\"\n"
                          "ln -s -f \\\n"
                          "  \"" "$XDG_CACHE_HOME/fontconfig\" \\\n"
                          "  \"" "$XDG_CACHE_HOME/gothic2/\"\n"
                          "XDG_CACHE_HOME=\"$XDG_CACHE_HOME/gothic2\" \\\n"
                          "  winetricks \\\n"
                          "    dmband \\\n"
                          "    dmcompos \\\n"
                          "    dmime \\\n"
                          "    dmloader \\\n"
                          "    dmstyle \\\n"
                          "    dmsynth \\\n"
                          "    dmusic\n\n"

                          "cd \"$XDG_DATA_HOME/gothic2\"\n"
                          "wine \"$XDG_DATA_HOME/gothic2/system/gothic2.exe\"\n")
                        port)))
                  (chmod (string-append out "/bin/gothic2") #o555))))
            (add-after 'install-wrapper 'install-desktop-entry
              (lambda* (#:key outputs #:allow-other-keys)
                (let ((out (assoc-ref outputs "out")))
                  (make-desktop-entry-file
                    (string-append out "/share/applications/gothic2.desktop")
                    #:name "Gothic II"
                    #:icon "gothic2"
                    #:exec (string-append out "/bin/gothic2")
                    #:categories '("Application" "Game"))))))
        #:install-plan
        ''(("data" "share/gothic2/")
           ("miles" "share/gothic2/")
           ("system" "share/gothic2/")
           ("_work" "share/gothic2/")
           ("vdfs.cfg" "share/gothic2/")
           ("manual.pdf" "share/doc/gothic2/")
           ("manual_addon.pdf" "share/doc/gothic2/manual-addon.pdf")
           ("icons/goggame-1207658718_1_16x16x32.png"
            "share/icons/hicolor/16x16/apps/gothic2.png")
           ("icons/goggame-1207658718_2_24x24x32.png"
            "share/icons/hicolor/24x24/apps/gothic2.png")
           ("icons/goggame-1207658718_3_32x32x32.png"
            "share/icons/hicolor/32x32/apps/gothic2.png")
           ("icons/goggame-1207658718_4_48x48x32.png"
            "share/icons/hicolor/48x48/apps/gothic2.png")
           ("icons/goggame-1207658718_5_64x64x32.png"
            "share/icons/hicolor/64x64/apps/gothic2.png")
           ("icons/goggame-1207658718_6_128x128x32.png"
            "share/icons/hicolor/128x128/apps/gothic2.png")
           ("icons/goggame-1207658718_7_256x256x32.png"
            "share/icons/hicolor/256x256/apps/gothic2.png"))))
    (home-page "https://gog.com/game/gothic_2_gold_edition")
    (synopsis "Gothic II")
    (description "Gothic II is a role-playing video game developed by a German company
Piranha Bytes and the sequel to Gothic I.  It was released on November 29, 2002 in Germany
and in North America on October 28, 2003.

Gothic II was a commercial success in Germany, and became JoWood's biggest hit at the time
of its release.  By 2004, it had sold over 300,000 copies when combined with its expansion
pack, Night of the Raven.

The events of Gothic II follow shortly after the finale of the original game.  When the
barrier around the prison colony was destroyed, ore supplies for the kingdom have stopped.
The king decides to send Lord Hagen with 100 paladins to the island to secure ore.  On
Khorinis, prisoners that escaped the camp raided the country.  Seeing as the militia was
unable to protect them, some farmers formed an alliance with the refugees and no longer
paid allegiance to the king.  Evil did not disappear with the Sleeper being banned, as
with his last cry, the Sleeper summoned the most evil creatures.  Xardas felt this and
rescued the Nameless Hero from under the ruins of the Sleeper's temple, where he had lain
for weeks, becoming weak.")
    (license (undistributable "No URL"))))
